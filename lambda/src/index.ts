// Main handler.
console.log('>> DynamoLambda main handler loaded....');
export default function handler(event, context, callback) {
  console.log('>> Received event = ', JSON.stringify(event, null, 2));

  console.log('>> Remaining time =', context.getRemainingTimeInMillis());
  console.log('>> functionName =', context.functionName);
  console.log('>> functionVersion =', context.functionVersion);
  console.log('>> invokedFunctionArn =', context.invokedFunctionArn);
  console.log('>> awsRequestId =', context.awsRequestId);
  console.log('>> logGroupName =', context.logGroupName);
  console.log('>> logStreamName =', context.logStreamName);
  console.log('>> clientContext =', context.clientContext);
  if (context.identity) {
    console.log('>> Cognito identity ID =', context.identity.cognitoIdentityId);
    console.log('>> Cognito identity Pool ID =', context.identity.cognitoIdentityPoolId);
  }

  // TO DO:
  // Your implementation here.
  // ...

  // Success.
  // callback(null, JSON.stringify(event));

  // Error
  callback("You have not implemented your Lambda function."); 

}
